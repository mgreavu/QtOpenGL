#ifndef JSONPARSER_H
#define JSONPARSER_H

#include "ModelLoader.h"
#include <json-c/json.h>
#include <string>

class SceneNode;

class JsonParser
{
public:
    JsonParser(SceneNode* root);

    bool Load(const std::string& fileName);

private:
    enum JsonSceneNodeFlags {
        HAS_NOTHING = 0,
        HAS_TRANSLATION = 1,
        HAS_ROTATION_EULER = 2,
        HAS_ROTATION_QUAT = 4
    };

    void LoadNode(json_object* rootJson, SceneNode* rootSceneNode);

    SceneNode* m_root;
    std::string m_modelsRootPath;
    ModelLoader m_loader;
};

#endif // JSONPARSER_H
