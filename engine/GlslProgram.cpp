#include "GlslProgram.h"
#include <fstream>

#define GLCheckError() ( glGetError() == GL_NO_ERROR )

GlslProgram::GlslProgram()
    : m_shaderProg(0)
{
    initializeOpenGLFunctions();
}

GlslProgram::~GlslProgram()
{
    // Delete the intermediate shader objects that have been added to the program
    // The list will only contain something if shaders were compiled but the object itself
    // was destroyed prior to linking
    for (auto it = m_shaders.begin(); it != m_shaders.end(); it++)
    {
        glDeleteShader(*it);
    }

    if (m_shaderProg != 0)
    {
        glDeleteProgram(m_shaderProg);
        m_shaderProg = 0;
    }
}

bool GlslProgram::Init()
{
    m_shaderProg = glCreateProgram();
    return m_shaderProg != 0;
}

// Use this method to add shaders to the program. When finished - call Finalize()
bool GlslProgram::AddShader(GLenum shaderType, const char* pFilename)
{
    std::string shaderSrc;
    if (!ReadShaderSource(pFilename, shaderSrc))
    {
        return false;
    }

    GLuint shaderObj = glCreateShader(shaderType);
    if (shaderObj == 0)
    {
        qDebug("Error creating shader type %d", shaderType);
        return false;
    }

    // Save the shader object - will be deleted in the destructor
    m_shaders.push_back(shaderObj);

    const GLchar* p[1];
    p[0] = shaderSrc.c_str();
    GLint Lengths[1] = { static_cast<GLint>(shaderSrc.size()) };
    glShaderSource(shaderObj, 1, p, Lengths);

    glCompileShader(shaderObj);

    GLint success = 0;
    glGetShaderiv(shaderObj, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        GLchar InfoLog[1024];
        glGetShaderInfoLog(shaderObj, 1024, nullptr, InfoLog);
        qDebug("Error compiling '%s': '%s'", pFilename, InfoLog);
        return false;
    }

    glAttachShader(m_shaderProg, shaderObj);
    return true;
}

// After all the shaders have been added to the program call this function to link and validate the program
bool GlslProgram::Finalize()
{
    GLchar ErrorLog[1024];
    memset(ErrorLog, 0, sizeof(ErrorLog));

    glLinkProgram(m_shaderProg);

    GLint success = 0;
    glGetProgramiv(m_shaderProg, GL_LINK_STATUS, &success);
    if (success == 0)
    {
        glGetProgramInfoLog(m_shaderProg, sizeof(ErrorLog), nullptr, ErrorLog);
        qDebug("Error linking shader program: '%s'", ErrorLog);
        return false;
    }

    glValidateProgram(m_shaderProg);
    glGetProgramiv(m_shaderProg, GL_VALIDATE_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(m_shaderProg, sizeof(ErrorLog), nullptr, ErrorLog);
        qDebug("Invalid shader program: '%s'\r\n", ErrorLog);
        return false;
    }

    // Delete the intermediate shader objects that have been added to the program
    for (auto it = m_shaders.begin(); it != m_shaders.end(); it++)
    {
        glDeleteShader(*it);
    }

    m_shaders.clear();
    return GLCheckError();
}

void GlslProgram::Enable()
{
    glUseProgram(m_shaderProg);
}

GLint GlslProgram::GetUniformLocation(const char* pUniformName)
{
    GLint Location = glGetUniformLocation(m_shaderProg, pUniformName);
    if (Location == static_cast<GLint>(INVALID_UNIFORM_LOCATION))
    {
        qDebug("Unable to get the location of uniform: '%s'", pUniformName);
    }
    return Location;
}

GLint GlslProgram::GetProgramParam(GLenum param)
{
    GLint ret = 0;
    glGetProgramiv(m_shaderProg, param, &ret);
    return ret;
}

bool GlslProgram::ReadShaderSource(const char* pFileName, std::string& outFile)
{
    std::ifstream f(pFileName);
    if (f.is_open())
    {
        std::string line;
        while (getline(f, line))
        {
            outFile.append(line);
            outFile.append("\n");
        }
        f.close();
        return true;
    }
    qDebug("Error reading shader file: %s", pFileName);
    return false;
}
