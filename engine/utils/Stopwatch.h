#ifndef _STOPWATCH_H_
#define _STOPWATCH_H_

#include <chrono>
#include <cstdint>

namespace graphics_engine
{
class Stopwatch
{
public:
    Stopwatch();

    void Start();
    uint64_t Restart();
    uint64_t Stop();
    uint64_t Elapsed() const; // milliseconds

private:
/*
    Class std::chrono::steady_clock represents a monotonic clock.
    The time points of this clock cannot decrease as physical time moves forward and the time between ticks of this clock is constant.
    This clock is not related to wall clock time (for example, it can be time since last reboot),
    and is most suitable for measuring intervals.
*/
    std::chrono::time_point<std::chrono::steady_clock> m_start;
    std::chrono::time_point<std::chrono::steady_clock> m_stop;
};

} // namespace rb_navigation_utils

#endif // _STOPWATCH_H_
