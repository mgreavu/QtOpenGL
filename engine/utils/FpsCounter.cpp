#include "FpsCounter.h"

graphics_engine::FpsCounter::FpsCounter()
    : m_frameCount(0)
    , m_start(std::chrono::steady_clock::now())
{
}

void graphics_engine::FpsCounter::Update()
{
    m_frameCount++;
}

uint64_t graphics_engine::FpsCounter::GetFps()
{
    const auto now = std::chrono::steady_clock::now();
    const double elapsed = std::chrono::duration_cast<std::chrono::duration<double, std::milli>>(now - m_start).count();
    const uint64_t fps = static_cast<uint64_t>(m_frameCount * 1000.0 / elapsed);
    m_start = now;
    m_frameCount = 0;
    return fps;
}
