#ifndef FPSCOUNTER_H
#define FPSCOUNTER_H

#include <chrono>

namespace graphics_engine
{
class FpsCounter
{
public:
    FpsCounter();

    void Update();
    uint64_t GetFps();

private:
    uint64_t m_frameCount;
    std::chrono::time_point<std::chrono::steady_clock> m_start;
};
}
#endif // FPSCOUNTER_H
