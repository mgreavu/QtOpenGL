#version 330

layout (location = 0) in vec3 PosCoord;
layout (location = 1) in vec2 TexCoord;

uniform vec3 gTopLeft; // worldspace
uniform vec2 gScale; // width and height scaling
uniform mat4 gViewProj;  // combined camera projection transform matrix

out vec2 TexCoord0;

void main()
{	
	// get the clip space position
	gl_Position = gViewProj * vec4(gTopLeft, 1.0f);

	// move the vertex in directly NDC space
	gl_Position.xy += PosCoord.xy * vec2(gScale.x, gScale.y);

	TexCoord0 = TexCoord;
}
