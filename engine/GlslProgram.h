#pragma once

#include <QOpenGLFunctions_3_3_Core>

#include <list>
#include <string>

class GlslProgram : protected QOpenGLFunctions_3_3_Core
{
public:
    GlslProgram();
    virtual ~GlslProgram();

    virtual bool Init();
    void Enable();

    enum { INVALID_UNIFORM_LOCATION = 0xffffffff };

protected:
    bool ReadShaderSource(const char* pFileName, std::string& outFile);
    bool AddShader(GLenum shaderType, const char* pFilename);
    bool Finalize();
    GLint GetUniformLocation(const char* pUniformName);
    GLint GetProgramParam(GLenum param);

    GLuint m_shaderProg;
    std::list<GLuint> m_shaders;
};
