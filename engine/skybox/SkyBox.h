#pragma once
#include "SkyboxProgram.h"

#include <QOpenGLFunctions_3_3_Core>

#include <string>

class SkyBox : protected QOpenGLFunctions_3_3_Core
{
public:
    SkyBox(const std::string& ext);
    ~SkyBox();

    bool Load(const std::string& path);
    void Render(const glm::mat4& mvp);

protected:
    const std::string m_ext;
    GLuint m_vboId;
    GLuint m_texId;
    SkyboxProgram m_program;
};

