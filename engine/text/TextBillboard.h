#pragma once
#include "TextBillboardProgram.h"
#include "engine/Vertex.h"

#include <glm/fwd.hpp>
#include <string>
#include <vector>
#include <memory>
#include <map>

#include <QOpenGLFunctions_3_3_Core>

class Texture2D;

class TextBillboard : protected QOpenGLFunctions_3_3_Core
{
public:
    TextBillboard();
    ~TextBillboard() override;

    bool Load();
    void SetViewport(const glm::i32vec2& size);
    void Render(const glm::mat4& view);

    void InsertLine(int lineNo, const std::string& text);

private:
    struct TextLine {
        TextLine(const std::string& text)
            : m_text(text)
            , m_vboId(0)
        {}

        std::string m_text;
        GLuint m_vboId;
        std::vector<Vertex> m_vertices;
    };
    std::map<int, TextLine> m_lines; // line number - TextLine pair

    TextBillboardProgram m_program;
    std::unique_ptr<Texture2D> m_texAtlas;
    GLuint m_texId;

    glm::vec2 m_viewport;
    glm::mat4 m_projection;
};

