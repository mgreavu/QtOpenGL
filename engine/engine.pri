win32-msvc {
    message("Using settings for Windows")
    INCLUDEPATH += d:\vcpkg\installed\x64-windows\include
    INCLUDEPATH += d:\vcpkg\installed\x64-windows\include\glm
    INCLUDEPATH += d:\vcpkg\installed\x64-windows\include\json-c
    INCLUDEPATH += d:\vcpkg\installed\x64-windows\include\assimp\

    LIBS += -Ld:\vcpkg\installed\x64-windows\lib\ -ljson-c
    LIBS += -Ld:\vcpkg\installed\x64-windows\lib\ -lFreeImage
    LIBS += -Ld:\vcpkg\installed\x64-windows\lib\ -lassimp-vc142-mt -lzlib
}

HEADERS += \
    $$PWD/SceneGraph.h \
    $$PWD/model3d/JsonParser.h \
    $$PWD/text/TextBillboard.h \
    $$PWD/text/TextBillboardProgram.h \
    $$PWD/Texture2D.h \
    $$PWD/skybox/SkyboxProgram.h \
    $$PWD/utils/FpsCounter.h \
    $$PWD/utils/Stopwatch.h \
    ../engine/BoundingBox.h \
    ../engine/BoundingSphere.h \
    ../engine/FastFrustumCulling.h \
    ../engine/GlslProgram.h \
    ../engine/Grid.h \
    ../engine/SceneNode.h \
    ../engine/Vertex.h \
    ../engine/camera/Projector.h \
    ../engine/camera/ProjectorFPS.h \
    ../engine/lights/DirectionalLight.h \
    ../engine/model3d/Material.h \
    ../engine/model3d/Mesh.h \
    ../engine/model3d/Model3d.h \
    ../engine/model3d/ModelLoader.h \
    ../engine/renderers/RenderFlags.h \
    ../engine/skybox/SkyBox.h

SOURCES += \
    $$PWD/SceneGraph.cpp \
    $$PWD/model3d/JsonParser.cpp \
    $$PWD/text/TextBillboard.cpp \
    $$PWD/text/TextBillboardProgram.cpp \
    $$PWD/Texture2D.cpp \
    $$PWD/skybox/SkyboxProgram.cpp \
    $$PWD/utils/FpsCounter.cpp \
    $$PWD/utils/Stopwatch.cpp \
    ../engine/FastFrustumCulling.cpp \
    ../engine/GlslProgram.cpp \
    ../engine/SceneNode.cpp \
    ../engine/camera/ProjectorFPS.cpp \
    ../engine/model3d/ModelLoader.cpp \
    ../engine/skybox/SkyBox.cpp
    
DISTFILES += \
    $$PWD/shaders/diffuse_specular.fs \
    $$PWD/shaders/diffuse_specular.vs \
    $$PWD/shaders/space.fs \
    $$PWD/shaders/space.vs \
    $$PWD/shaders/billboard_ui.fs \
    $$PWD/shaders/billboard_ui.vs \
    ../engine/shaders/skybox.fs \
    ../engine/shaders/skybox.vs \
    ../engine/shaders/basic.fs \
    ../engine/shaders/basic.vs
