#include "MainWindow.h"

#include <QApplication>
#include <QStyleFactory>
#include <QSurfaceFormat>

#include <FreeImage.h>

int main(int argc, char *argv[])
{
    FreeImage_Initialise(false);

    QApplication a(argc, argv);

    QSurfaceFormat format;
    format.setRedBufferSize(8);
    format.setGreenBufferSize(8);
    format.setBlueBufferSize(8);
    format.setDepthBufferSize(16);
    format.setStencilBufferSize(8);
    format.setSamples(4);
    format.setSwapInterval(0);
/*
    The 3.0 form of context creation did not have the concept of profiles. There was only one form of OpenGL: the core specification.
    In 3.2, OpenGL was effectively split into two profiles, core and compatibility. An implementation was only required to define core,
    so compatibility is not guaranteed to be available. However, on most implementations, the compatibility profile will be available.
*/
    format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setSwapBehavior(QSurfaceFormat::DefaultSwapBehavior);
    QSurfaceFormat::setDefaultFormat(format);

    MainWindow w;
    w.show();
    const int res = a.exec();
    FreeImage_DeInitialise();
    return res;
}
