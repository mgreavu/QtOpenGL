#include "GlslProgramLight.h"
#include <glm/gtc/type_ptr.hpp>

bool GlslProgramLight::Init()
{
    bool ok = GlslProgram::Init();
    ok = ok && AddShader(GL_VERTEX_SHADER, "shaders/space.vs");

    glBindAttribLocation(m_shaderProg, 0, "Position");
    glBindAttribLocation(m_shaderProg, 1, "TexCoord");
    glBindAttribLocation(m_shaderProg, 2, "Normal");

    ok = ok && AddShader(GL_FRAGMENT_SHADER, "shaders/space.fs");
    ok = ok && Finalize();

    if (ok)
    {
        m_locMVP = GetUniformLocation("gWVP");
        m_locWorldMatrix = GetUniformLocation("gWorld");
        m_locSampler = GetUniformLocation("gSampler");
        m_locHasTexture = GetUniformLocation("gHasTexture");

        m_locDirLightColor = GetUniformLocation("gDirectionalLight.Color");
        m_locDirLightAmbientIntensity = GetUniformLocation("gDirectionalLight.AmbientIntensity");
        m_locDirLightDirection = GetUniformLocation("gDirectionalLight.Direction");
        m_locDirLightDiffuseIntensity = GetUniformLocation("gDirectionalLight.DiffuseIntensity");

        if (m_locMVP == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locWorldMatrix == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locSampler == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locHasTexture == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locDirLightAmbientIntensity == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locDirLightColor == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locDirLightDirection == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locDirLightDiffuseIntensity == static_cast<GLint>(INVALID_UNIFORM_LOCATION))
        {
            ok = false;
        }
    }
    return ok;
}

void GlslProgramLight::SetMVP(const glm::mat4& mvp)
{
    glUniformMatrix4fv(m_locMVP, 1, GL_FALSE, glm::value_ptr(mvp));
}

void GlslProgramLight::SetWorldMatrix(const glm::mat4& world)
{
    glUniformMatrix4fv(m_locWorldMatrix, 1, GL_FALSE, glm::value_ptr(world));
}

void GlslProgramLight::SetHasTexture(bool hasTexture)
{
    glUniform1i(m_locHasTexture, hasTexture);
}

void GlslProgramLight::SetTextureUnit(GLint textureUnit)
{
    glUniform1i(m_locSampler, textureUnit);
}

void GlslProgramLight::SetDirectionalLight(const DirectionalLight& light)
{
    glUniform3f(m_locDirLightColor, light.m_color.x, light.m_color.y, light.m_color.z);
    glUniform1f(m_locDirLightAmbientIntensity, light.m_ambientIntensity);
    glm::vec3 direction = glm::normalize(light.m_direction);
    glUniform3f(m_locDirLightDirection, direction.x, direction.y, direction.z);
    glUniform1f(m_locDirLightDiffuseIntensity, light.m_diffuseIntensity);
}

void GlslProgramLight::SetColor(const glm::vec3& color)
{
    glUniform3f(m_locDirLightColor, color.x, color.y, color.z);
}
