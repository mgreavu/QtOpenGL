#include "CanvasWidget.h"
#include "engine/camera/ProjectorFPS.h"
#include "engine/SceneGraph.h"
#include "engine/model3d/Model3d.h"
#include "SGRendererGL.h"

#include <QKeyEvent>

static constexpr float MOVEMENT_LARGE = 50.0f;
static constexpr float MOVEMENT_SMALL = 1.0f;

CanvasWidget::CanvasWidget(QWidget *parent)
    : QOpenGLWidget(parent)
    , m_camera(std::make_shared<ProjectorFPS>(45.0f, 0.2f, 5000.0f))
    , m_sceneGraph(nullptr)
    , m_renderer(nullptr)
{
    setFocusPolicy(Qt::StrongFocus);

    // For a small performance improvement, we set the widget's Qt::WA_NoSystemBackground attribute
    // to instruct the underlying window system not to paint a background for the widget.
    setAttribute(Qt::WA_NoSystemBackground, true);
}

/*
    Note 1: std::unique_ptr<SGRendererGL> m_renderer;
    Instantiation happens where you do the specialization (in this case the header).
    The compiler needs the members to be fully defined to generate the default destructor (i.e. it needs to know
    the destructor of CanvasWidget in this case) and this is happening while parsing the header no less.

    Note 2: The QOpenGLWidget's context is made current in the destructor, allowing for safe destruction
    of any child object that may need to release OpenGL resources belonging to the context provided by this widget.
*/
CanvasWidget::~CanvasWidget() = default;

void CanvasWidget::initializeGL()
{
    initializeOpenGLFunctions();

    DbgPrintFormat();

    m_camera->SetPosition(glm::vec3(0.0f, 50.0f, 550.0f));
    m_camera->Update();

    m_renderer = std::make_unique<SGRendererGL>();
    m_renderer->Init(m_camera);
    m_renderer->SetCullMode(CullMode::CULL_BACK);
}

void CanvasWidget::resizeGL(int w, int h)
{
    m_renderer->Resize(w, h);
    m_camera->Update();
}

void CanvasWidget::paintGL()
{
    m_renderer->Render();
    m_fpsCounter.Update();
}

void CanvasWidget::SetScene(std::shared_ptr<SceneGraph> sceneGraph)
{
    m_sceneGraph = sceneGraph;
    m_renderer->SetScene(m_sceneGraph);
}

void CanvasWidget::Update()
{
    update();
}

void CanvasWidget::wheelEvent(QWheelEvent *event)
{
    (event->angleDelta().y() > 0) ? m_camera->MoveForward(5.0f) : m_camera->MoveBackward(5.0f);
    m_camera->Update();
    update();
}

void CanvasWidget::mousePressEvent(QMouseEvent *event)
{
    const Qt::MouseButtons buttons = event->buttons();
    if (buttons & Qt::LeftButton)
    {
        m_PosStart = glm::vec2(event->pos().x(), event->pos().y());
        m_headingStart = m_camera->GetHeading();
        m_tiltingStart = m_camera->GetTilting();
    }

    if (m_sceneGraph)
    {
        const glm::vec3 rayDir = m_camera->Raycast(glm::i32vec2(event->pos().x(), event->pos().y()));
        const glm::vec3& rayOrigin = m_camera->GetCameraPosition();
        float distNearest = std::numeric_limits<float>::max();
        SceneNode* pNearest = nullptr;
        m_sceneGraph->Select(rayOrigin, rayDir, distNearest, &pNearest);
        if (pNearest)
        {
            qDebug("Nearest found at dist: %.4f", static_cast<double>(distNearest));
            makeCurrent();

            char textline[128];
            memset(textline, 0, 128);
            snprintf(textline, 128, "Selected: %s", pNearest->GetModel()->GetName().c_str());
            m_renderer->InsertText(1, textline);

            memset(textline, 0, 128);
            snprintf(textline, 128, "Distance: %.2f (%.2f km)", distNearest, distNearest * 1000.0f);
            m_renderer->InsertText(2, textline);
        }
    }
}

void CanvasWidget::mouseMoveEvent(QMouseEvent *event)
{
    const Qt::MouseButtons buttons = event->buttons();
    if (buttons & Qt::LeftButton)
    {
        glm::vec2 posCurrent = glm::vec2(event->pos().x(), event->pos().y());
        glm::vec2 mouseDelta = posCurrent - m_PosStart;

        const glm::i32vec2& size = m_camera->GetViewport();

        m_camera->SetHeading(m_headingStart + 360.0f * (mouseDelta.x / size.x));
        m_camera->SetTilting(m_tiltingStart + 90.0f * (mouseDelta.y / size.y));
        m_camera->Update();
        update();
    }
}

void CanvasWidget::keyPressEvent(QKeyEvent *event)
{
    const bool shiftDown = event->modifiers() & Qt::ShiftModifier;
    switch(event->key())
    {
    case Qt::Key_Up: {
        m_camera->MoveBackward(shiftDown ? MOVEMENT_SMALL : MOVEMENT_LARGE);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_Down: {
        m_camera->MoveForward(shiftDown ? MOVEMENT_SMALL : MOVEMENT_LARGE);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_Left: {
        m_camera->StrafeLeft(shiftDown ? MOVEMENT_SMALL : MOVEMENT_LARGE);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_Right: {
        m_camera->StrafeRight(shiftDown ? MOVEMENT_SMALL : MOVEMENT_LARGE);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_PageUp: {
        m_camera->MoveUp(shiftDown ? MOVEMENT_SMALL : MOVEMENT_LARGE);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_PageDown: {
        m_camera->MoveDown(shiftDown ? MOVEMENT_SMALL : MOVEMENT_LARGE);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_A: {
        float angle = m_camera->GetHeading();
        angle += 2.0f;
        m_camera->SetHeading(angle);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_D: {
        float angle = m_camera->GetHeading();
        angle -= 2.0f;
        m_camera->SetHeading(angle);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_W: {
        float angle = m_camera->GetTilting();
        angle += 2.0f;
        m_camera->SetTilting(angle);
        m_camera->Update();
        update();
        break;
    }
    case Qt::Key_S: {
        float angle = m_camera->GetTilting();
        angle -= 2.0f;
        m_camera->SetTilting(angle);
        m_camera->Update();
        update();
        break;       
    }
    case Qt::Key_B: {
        makeCurrent();
        const RenderingFlags flags = m_renderer->GetFlags();
        if (flags & RenderingFlags::FLAG_DRAW_BBOX)
        {
            m_renderer->RemoveFlag(RenderingFlags::FLAG_DRAW_BBOX);
        }
        else
        {
            m_renderer->AddFlag(RenderingFlags::FLAG_DRAW_BBOX);
        }
        update();
        break;
    }
    case Qt::Key_T: {
        makeCurrent();
        const RenderingFlags flags = m_renderer->GetFlags();
        if (flags & RenderingFlags::FLAG_ENBLE_TEXTURES)
        {
            m_renderer->RemoveFlag(RenderingFlags::FLAG_ENBLE_TEXTURES);
        }
        else
        {
            m_renderer->AddFlag(RenderingFlags::FLAG_ENBLE_TEXTURES);
        }
        update();
        break;
    }
    case Qt::Key_F: {
        makeCurrent();
        const RenderingFlags flags = m_renderer->GetFlags();
        if (flags & RenderingFlags::FLAG_DRAW_WIREFRAME)
        {
            m_renderer->RemoveFlag(RenderingFlags::FLAG_DRAW_WIREFRAME);
        }
        else
        {
            m_renderer->AddFlag(RenderingFlags::FLAG_DRAW_WIREFRAME);
        }
        update();
        break;
    }
    default: {
        qDebug("Key has no function");
        break;
    }
    } // end switch
}

uint64_t CanvasWidget::DbgGetFps()
{
    return m_fpsCounter.GetFps();
}

void CanvasWidget::DbgPrintFormat()
{
    qDebug("Vendor: %s", glGetString(GL_VENDOR));
    qDebug("Renderer: %s", glGetString(GL_RENDERER));
    qDebug("Version: %s", glGetString(GL_VERSION));

    GLint value;
    // the max width and height of a 1D or 2D texture that your GPU supports
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &value);
    qDebug("GL_MAX_TEXTURE_SIZE: (%d x %d)", value, value);

    // the max width, height, depth of a 3D texture that your GPU supports
    glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &value);
    qDebug("GL_MAX_3D_TEXTURE_SIZE: (%d x %d x %d)", value, value, value);

    // the max width and height of a a cubemap texture that your GPU supports
    glGetIntegerv(GL_MAX_CUBE_MAP_TEXTURE_SIZE, &value);
    qDebug("GL_MAX_CUBE_MAP_TEXTURE_SIZE: (%d x %d)", value, value);

    // the number of image samplers that your GPU supports in the fragment shader
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &value);
    qDebug("GL_MAX_TEXTURE_IMAGE_UNITS: %d", value);

    glGetIntegerv(GL_PACK_ALIGNMENT, &value);
    qDebug("GL_PACK_ALIGNMENT: %d", value);

    glGetIntegerv(GL_UNPACK_ALIGNMENT, &value);
    qDebug("GL_UNPACK_ALIGNMENT: %d", value);

    const QSurfaceFormat surfaceFormat = format();
    qDebug("QSurfaceFormat:");
    qDebug("\tVersion: %d.%d", surfaceFormat.majorVersion(), surfaceFormat.minorVersion());
    const QSurfaceFormat::OpenGLContextProfile profile = surfaceFormat.profile();
    switch(profile)
    {
        case QSurfaceFormat::NoProfile:
        {
            qDebug("\tQSurfaceFormat::NoProfile");
            break;
        }
        case QSurfaceFormat::CoreProfile:
        {
            qDebug("\tQSurfaceFormat::CoreProfile");
            break;
        }
        case QSurfaceFormat::CompatibilityProfile:
        {
            qDebug("\tQSurfaceFormat::CompatibilityProfile");
            break;
        }
    }

    const QSurfaceFormat::FormatOptions options = surfaceFormat.options();
    switch(options)
    {
        case QSurfaceFormat::StereoBuffers:
        {
            qDebug("\tQSurfaceFormat::StereoBuffers");
            break;
        }
        case QSurfaceFormat::DebugContext:
        {
            qDebug("\tQSurfaceFormat::DebugContext");
            break;
        }
        case QSurfaceFormat::DeprecatedFunctions:
        {
            qDebug("\tQSurfaceFormat::DeprecatedFunctions");
            break;
        }
    }

    const QSurfaceFormat::RenderableType renderableType = surfaceFormat.renderableType();
    switch(renderableType)
    {
        case QSurfaceFormat::DefaultRenderableType:
        {
            qDebug("\tQSurfaceFormat::DefaultRenderableType");
            break;
        }
        case QSurfaceFormat::OpenGL:
        {
            qDebug("\tQSurfaceFormat::OpenGL");
            break;
        }
        case QSurfaceFormat::OpenGLES:
        {
            qDebug("\tQSurfaceFormat::OpenGLES");
            break;
        }
        case QSurfaceFormat::OpenVG:
        {
            qDebug("\tQSurfaceFormat::OpenVG");
            break;
        }
    }

    const QSurfaceFormat::SwapBehavior behavior = surfaceFormat.swapBehavior();
    switch(behavior)
    {
        case QSurfaceFormat::DefaultSwapBehavior:
        {
            qDebug("\tQSurfaceFormat::DefaultSwapBehavior");
            break;
        }
        case QSurfaceFormat::SingleBuffer:
        {
            qDebug("\tQSurfaceFormat::SingleBuffer");
            break;
        }
        case QSurfaceFormat::DoubleBuffer:
        {
            qDebug("\tQSurfaceFormat::DoubleBuffer");
            break;
        }
        case QSurfaceFormat::TripleBuffer:
        {
            qDebug("\tQSurfaceFormat::TripleBuffer");
            break;
        }
    }
}
