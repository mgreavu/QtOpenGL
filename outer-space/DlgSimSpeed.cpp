#include "DlgSimSpeed.h"
#include "ui_DlgSimSpeed.h"

DlgSimSpeed::DlgSimSpeed(QWidget *parent, float factor)
    : QDialog(parent)
    , ui(new Ui::DlgSimSpeed)
    , m_factor(factor)
{
    ui->setupUi(this);
    setWindowTitle("Simulation Speed");
    setFixedSize(width(), height());

    ui->doubleSpinBox->setValue(m_factor);
    ui->doubleSpinBox->setFocus();
}

DlgSimSpeed::~DlgSimSpeed()
{
    delete ui;
}

void DlgSimSpeed::accept()
{
    m_factor = static_cast<float>(ui->doubleSpinBox->value());
    QDialog::accept();
}
