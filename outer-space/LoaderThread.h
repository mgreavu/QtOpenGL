#ifndef LOADERTHREAD_H
#define LOADERTHREAD_H

#include "engine/SceneGraph.h"
#include "engine/model3d/JsonParser.h"

#include <QThread>
#include <QObject>

#include <memory>

class LoaderThread : public QThread
{
    Q_OBJECT
public:
    explicit LoaderThread(QObject *parent, std::shared_ptr<SceneGraph> sceneGraph)
        : QThread(parent)
        , m_sceneGraph(sceneGraph)
    {}

    void run() override
    {
        qDebug("LoaderThread::run() thread id: %p", QThread::currentThreadId());
        JsonParser jsonParser(m_sceneGraph->GetRoot());
        const bool ok = jsonParser.Load("jovian_system.json");
        emit loadFinished(ok);
    }

signals:
    void loadFinished(bool success);

private:
    std::shared_ptr<SceneGraph> m_sceneGraph;
};

#endif // LOADERTHREAD_H
