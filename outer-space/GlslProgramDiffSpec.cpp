#include "GlslProgramDiffSpec.h"
#include <glm/gtc/type_ptr.hpp>

bool GlslProgramDiffSpec::Init()
{
    bool ok = GlslProgram::Init();
    ok = ok && AddShader(GL_VERTEX_SHADER, "shaders/diffuse_specular.vs");

    glBindAttribLocation(m_shaderProg, 0, "aPos");
    glBindAttribLocation(m_shaderProg, 1, "aTexCoord");
    glBindAttribLocation(m_shaderProg, 2, "aNormal");

    ok = ok && AddShader(GL_FRAGMENT_SHADER, "shaders/diffuse_specular.fs");
    ok = ok && Finalize();

    if (ok)
    {
        m_locModel = GetUniformLocation("model");
        m_locView = GetUniformLocation("view");
        m_locProjection = GetUniformLocation("projection");

        m_locViewPos = GetUniformLocation("viewPos");

        m_locLightPosition = GetUniformLocation("light.position");
        m_locLightAmbient = GetUniformLocation("light.ambient");
        m_locLightDiffuse = GetUniformLocation("light.diffuse");
        m_locLightSpecular = GetUniformLocation("light.specular");

        m_locHasDiffuseTexture = GetUniformLocation("material.useDiffuseTex");
        m_locSamplerDiffuse = GetUniformLocation("material.diffuse");
        m_locHasSpecularTexture = GetUniformLocation("material.useSpecularTex");
        m_locSamplerSpecular = GetUniformLocation("material.specular");
        m_locShininess = GetUniformLocation("material.shininess");

        if (m_locModel == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locView == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locProjection == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locViewPos == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locLightPosition == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locLightAmbient == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locLightDiffuse == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locLightSpecular == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locHasDiffuseTexture == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locSamplerDiffuse == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locHasSpecularTexture == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locSamplerSpecular == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locShininess == static_cast<GLint>(INVALID_UNIFORM_LOCATION))
        {
            ok = false;
        }
    }
    return ok;
}

void GlslProgramDiffSpec::SetModel(const glm::mat4& model)
{
    glUniformMatrix4fv(m_locModel, 1, GL_FALSE, glm::value_ptr(model));
}

void GlslProgramDiffSpec::SetView(const glm::mat4 &view)
{
    glUniformMatrix4fv(m_locView, 1, GL_FALSE, glm::value_ptr(view));
}

void GlslProgramDiffSpec::SetProjection(const glm::mat4& projection)
{
    glUniformMatrix4fv(m_locProjection, 1, GL_FALSE, glm::value_ptr(projection));
}

void GlslProgramDiffSpec::SetViewPos(const glm::vec3& viewPos)
{
    glUniform3f(m_locViewPos, viewPos.x, viewPos.y, viewPos.z);
}

void GlslProgramDiffSpec::SetLightPos(const glm::vec3& pos)
{
    glUniform3f(m_locLightPosition, pos.x, pos.y, pos.z);
}

void GlslProgramDiffSpec::SetLightAmbient(const glm::vec3& ambient)
{
    glUniform3f(m_locLightAmbient, ambient.x, ambient.y, ambient.z);
}

void GlslProgramDiffSpec::SetLightDiffuse(const glm::vec3& diffuse)
{
    glUniform3f(m_locLightDiffuse, diffuse.x, diffuse.y, diffuse.z);
}

void GlslProgramDiffSpec::SetLightSpecular(const glm::vec3& specular)
{
    glUniform3f(m_locLightSpecular, specular.x, specular.y, specular.z);
}

void GlslProgramDiffSpec::SetUseDiffuseTexture(bool hasTexture)
{
    glUniform1i(m_locHasDiffuseTexture, hasTexture);
}

void GlslProgramDiffSpec::SetDiffuseSampler(GLint sampler)
{
    glUniform1i(m_locSamplerDiffuse, sampler);
}

void GlslProgramDiffSpec::SetUseSpecularTexture(bool hasTexture)
{
    glUniform1i(m_locHasSpecularTexture, hasTexture);
}

void GlslProgramDiffSpec::SetSpecularSampler(GLint sampler)
{
    glUniform1i(m_locSamplerSpecular, sampler);
}

void GlslProgramDiffSpec::SetSpecularShininess(GLfloat shininess)
{
    glUniform1f(m_locShininess, shininess);
}
