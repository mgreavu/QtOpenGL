#pragma once
#include "engine/GlslProgram.h"
#include "engine/lights/DirectionalLight.h"
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"


class GlslProgramLight : public GlslProgram
{
public:
    GlslProgramLight() = default;
    ~GlslProgramLight() override = default;

    bool Init() override;

    void SetMVP(const glm::mat4& mvp);
    void SetWorldMatrix(const glm::mat4& world);
    void SetTextureUnit(GLint textureUnit);
    void SetHasTexture(bool hasTexture);
    void SetDirectionalLight(const DirectionalLight& light);
    void SetColor(const glm::vec3& color);

private:
    GLint m_locMVP;
    GLint m_locWorldMatrix;
    GLint m_locSampler;
    GLint m_locHasTexture;
    GLint m_locDirLightColor;
    GLint m_locDirLightAmbientIntensity;
    GLint m_locDirLightDirection;
    GLint m_locDirLightDiffuseIntensity;
};
