QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 5): QT += openglwidgets

CONFIG += c++14
# CONFIG -= debug_and_release debug_and_release_target
CONFIG -= debug_and_release

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += GLM_ENABLE_EXPERIMENTAL
DEFINES += MODEL_STATS
#DEFINES += DEVELOPER_DEBUG

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ../
win32-msvc {
    message("Using settings for Windows")
    INCLUDEPATH += d:\vcpkg\installed\x64-windows\include\glm
}

linux-g++ {
    message("Using settings for Linux")
    LIBS += -L/usr/lib/x86_64-linux-gnu/ -ljson-c -lfreeimage -lassimp
}

include(../engine/engine.pri)

SOURCES += \
    CanvasWidget.cpp \
    GlslProgramBasic.cpp \
    GlslProgramDiffSpec.cpp \
    JovianSystemController.cpp \
    OrbitsDrawer.cpp \
    main.cpp \
    MainWindow.cpp \
    DlgSimSpeed.cpp \
    SGRendererGL.cpp

HEADERS += \
    CanvasWidget.h \
    GlslProgramBasic.h \
    GlslProgramDiffSpec.h \
    JovianConstants.h \
    JovianSystemController.h \
    LoaderThread.h \
    MainWindow.h \
    DlgSimSpeed.h \
    OrbitsDrawer.h \
    SGRendererGL.h

FORMS += \
    MainWindow.ui \
    DlgSimSpeed.ui
