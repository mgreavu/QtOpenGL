#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "engine/SceneGraph.h"
#include "JovianSystemController.h"
#include "CanvasWidget.h"
#include "LoaderThread.h"
#include "DlgSimSpeed.h"
#include <QCloseEvent>
#include <QMessageBox>

static constexpr int DESKTOP_DISPLAY_WIDTH { 1024 };
static constexpr int DESKTOP_DISPLAY_HEIGHT { 640 };

static constexpr int FPS_UPDATE_INTERVAL { 1000 };
static constexpr int SIMULATION_UPDATE_INTERVAL { 33 };
static constexpr int RENDER_UPDATE_INTERVAL { 10 };

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_sceneGraph(std::make_shared<SceneGraph>())
    , m_jsc(std::make_unique<sim_control::JovianSystemController>(m_sceneGraph))
    , m_timerIdFps(-1)
    , m_timerIdSimulation(-1)
    , m_timerIdRender(-1)
{
    qDebug("MainWindow::MainWindow() thread id: %p", QThread::currentThreadId());

    ui->setupUi(this);

    m_canvas = new CanvasWidget(this);

    m_fpsLabel = new QLabel("FPS: 0");
    ui->statusbar->addPermanentWidget(m_fpsLabel);

    setCentralWidget(m_canvas);
    // take into account the heights of the QMenuBar and of the QStatusBar
    resize(DESKTOP_DISPLAY_WIDTH, DESKTOP_DISPLAY_HEIGHT + ui->menubar->size().height() + ui->statusbar->size().height());

    QSizePolicy sp = m_canvas->sizePolicy();
    sp.setHorizontalPolicy(QSizePolicy::Expanding);
    sp.setVerticalPolicy(QSizePolicy::Expanding);

    m_timerIdFps = startTimer(FPS_UPDATE_INTERVAL);
    m_timerIdRender = startTimer(RENDER_UPDATE_INTERVAL);

    LoaderThread *loaderThread = new LoaderThread(this, m_sceneGraph);
    connect(loaderThread, &LoaderThread::loadFinished, this, &MainWindow::OnLoadFinished);
    connect(loaderThread, &LoaderThread::finished, loaderThread, &QObject::deleteLater);
    loaderThread->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (QMessageBox::question(this, "QtOpenGL", "Exit program ?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::No)
    {
        event->ignore();
        return;
    }

    if (m_timerIdSimulation != -1)
    {
        killTimer(m_timerIdSimulation);
    }
    killTimer(m_timerIdRender);
    killTimer(m_timerIdFps);
    event->accept();
}

void MainWindow::OnLoadFinished(bool success)
{
    qDebug("MainWindow::OnLoadFinished() thread id: %p", QThread::currentThreadId());
    if (success)
    {
        m_canvas->SetScene(m_sceneGraph);
        m_timerIdSimulation = startTimer(SIMULATION_UPDATE_INTERVAL);
    }
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    const int timerId = event->timerId();
    if (timerId == m_timerIdFps)
    {
        const QString fps = QString("FPS: %1").arg(QString::number(m_canvas->DbgGetFps()));
        m_fpsLabel->setText(fps);
    }
    else if (timerId == m_timerIdSimulation)
    {
        m_jsc->Update();
    }
    else if (timerId == m_timerIdRender)
    {
        m_canvas->Update();
    }
    else
    {
        qDebug("Unknown timer event with id: %d", timerId);
    }
}

void MainWindow::on_actionSpeed_triggered()
{
    DlgSimSpeed dlgSimSpeed(this, m_jsc->GetSimulationSpeed());
    if (dlgSimSpeed.exec() == QDialog::Accepted)
    {
        m_jsc->SetSimulationSpeed(dlgSimSpeed.GetFactor());
    }
}
