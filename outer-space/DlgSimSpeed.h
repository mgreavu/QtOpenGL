#ifndef DLGSIMSPEED_H
#define DLGSIMSPEED_H

#include <QDialog>

namespace Ui {
class DlgSimSpeed;
}

class DlgSimSpeed : public QDialog
{
    Q_OBJECT

public:
    explicit DlgSimSpeed(QWidget *parent, float factor);
    ~DlgSimSpeed();

    inline float GetFactor() const { return m_factor; }

private slots:
    void accept();

private:
    Ui::DlgSimSpeed *ui;
    float m_factor;
};

#endif // DLGSIMSPEED_H
