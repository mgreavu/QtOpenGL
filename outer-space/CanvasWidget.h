#ifndef CANVASWIDGET_H
#define CANVASWIDGET_H

#include "engine/utils/FpsCounter.h"
#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <glm/vec2.hpp>
#include <memory>


class Projector;
class SceneNode;
class SceneGraph;
class SGRendererGL;

class CanvasWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core
{
    Q_OBJECT
public:
    explicit CanvasWidget(QWidget *parent);
    ~CanvasWidget() override;

    void SetScene(std::shared_ptr<SceneGraph> sceneGraph);
    void Update();

    uint64_t DbgGetFps();

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void resizeGL(int w, int h) Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;

    void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

private:
    void DbgPrintFormat();

    std::shared_ptr<Projector> m_camera;
    std::shared_ptr<SceneGraph> m_sceneGraph;
    std::unique_ptr<SGRendererGL> m_renderer;

    glm::vec2  m_PosStart;
    float      m_headingStart;
    float      m_tiltingStart;
    graphics_engine::FpsCounter m_fpsCounter;
};

#endif // CANVASWIDGET_H
