#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>

#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

namespace sim_control
{
    class JovianSystemController;
}

class CanvasWidget;
class SceneGraph;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);
    void timerEvent(QTimerEvent *event);

private slots:
    void OnLoadFinished(bool success);

    void on_actionSpeed_triggered();

private:
    Ui::MainWindow *ui;
    QLabel* m_fpsLabel;

    std::shared_ptr<SceneGraph> m_sceneGraph;
    std::unique_ptr<sim_control::JovianSystemController> m_jsc;
    CanvasWidget *m_canvas;
    int m_timerIdFps;
    int m_timerIdSimulation;
    int m_timerIdRender;
};
#endif // MAINWINDOW_H
